import request from 'supertest';
import { app } from '../../app';

it('returns a 201 on a successful signup', async () => {
  return request(app)
    .post('/api/users/signup')
    .send({
      email: 'quangthaii056@gmail.com',
      password: '056Quang Thaii',
    })
    .expect(201);
});

it('returns a 400 with an invalid email', async () => {
  return request(app)
    .post('/api/users/signup')
    .send({
      email: 'test',
      password: '056Quang Thaii',
    })
    .expect(400);
});

it('returns a 400 with a invalid password', async () => {
  return request(app)
    .post('/api/users/signup')
    .send({
      email: 'quangthaii056@gmail.com',
      password: '',
    })
    .expect(400);
});

it('return a 400 with missing email and password', async () => {
  return request(app).post('/api/users/signup').send({}).expect(400);
});

it('disallow duplicate emails', async () => {
  await request(app).post('/api/users/signup').send({});
});
